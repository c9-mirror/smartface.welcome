define(function (require, exports, module) {
	function main(options, imports, register) {
		var Editor = imports.Editor,
		editors = imports.editors,
		ui = imports.ui,
		fs = imports.fs,
		commands = imports.commands,
		layout = imports.layout,
		tabManager = imports.tabManager,
		settings = imports.settings,
		container,
		handle,
		defaults = {
			"flat-light" : "#F8FDFF",
			"light" : "#b7c9d4",
			"light-gray" : "#b7c9d4",
			"dark" : "#203947",
			"dark-gray" : "#203947"
		},
		loaded = false;

		function smartfaceWelcome() {
			var plugin = new Editor("Smartface", main.consumes, []);

			plugin.on("draw", function (e) {
				container = e.htmlNode;
				container.style.paddingTop = "1px";
				var html = '<iframe id="welcomeSmartface" src="https://az793023.vo.msecnd.net/c9-dashboard/welcome/general.html" style="width:100%;height:100%;border:0;"></iframe>';
				ui.insertHtml(container, html, plugin);
			});

			plugin.on("load", function () {});
			plugin.on("documentLoad", function (e) {
				var doc = e.doc,
				tab = doc.tab,
				paddingTop;

				function setTheme(e) {
					var isDark = e.theme === "dark";
					if (isDark) {
						tab.backgroundColor = "#06454A";
						paddingTop = "1px";
					} else {
						tab.backgroundColor = "#f1f2f7";
						paddingTop = "0px";
					}
					if (isDark) {
						tab.classList.add("dark");
					} else {
						tab.classList.remove("dark");
					}
					container.style.paddingTop = paddingTop;
				}

				layout.on("themeChange", setTheme, doc);
				setTheme({
					theme : settings.get("user/general/@skin")
				});
				doc.title = "Welcome";
				doc.meta.welcome = true;
			});

			return plugin;
		}
		handle = editors.register("welcome", "URL Viewer", smartfaceWelcome, []);
		function load() {
			if (loaded) {
				return false;
			}
			loaded = true;
			function search() {
				var found, tabs = tabManager.getTabs();
				tabs.every(function (tab) {
					if (tab.document.meta.welcome) {
						found = tab;
						return false;
					}
					return true;
				});
				return found;
			}
			function show(cb) {
				var tab = search();
				if (tab) {
					return tabManager.focusTab(tab);
				}

				tabManager.open({
					editorType : "welcome",
					noanim : true,
					active : true
				}, cb);
			}
			tabManager.once("ready", function () {
				settings.on("read", function (e) {
					if (e.reset) {
						settings.set("user/welcome/@first", true);
						return;
					}

					if (!settings.getBool("user/welcome/@first")) {
						show(function () {
							settings.set("user/welcome/@first", true);
						});
					}
				}, handle);

				if (window.location.hash.match(/#openfile-(.*)/)) {
					var file = "/" + RegExp.$1;
					fs.exists(file, function (exists) {
						if (!exists) {
							return;
						}
						commands.exec("preview", null, {
							path : file,
							focus : options.focusOpenFile || false
						});
					});
				}
			}, handle);
		}

		handle.on("load", load);

		/***** Methods *****/

		commands.addCommand({
			name : "smf.welcome",
			isAvailable : function () {
				return true;
			},
			exec : function () {
				tabManager.openEditor("welcome", true, function () {});
			}
		}, handle);

		ui.insertByIndex(layout.getElement('barTools'),
			new ui.button({
				id : "btnSmartfaceWelcome",
				skin : "c9-toolbarbutton-glossy",
				command : "smf.welcome",
				caption : "Welcome",
				disabled : false,
				"class" : "emulatorBtn"
			}), 998, handle);

		register(null, {
			welcome : handle
		});
	}

	main.consumes = [
		"Editor", "editors", "ui", "tabManager", "settings",
		"commands", "fs", "layout"
	];
	main.provides = ["welcome"];
	main.setup = "";
	return main;

	/*
	Change Ace Theme
	- Get List from Ace
	 */

});
